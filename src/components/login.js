import React, {Component} from 'react';
import {withCookies} from 'react-cookie';

class Login extends Component{

    state = {
        credentials: {
            username: '',
            password: ''
        },
        isLogin: true
    };

    inputChanged = event => {
        let credentials = this.state.credentials;
        credentials[event.target.name] = event.target.value;
        this.setState({credentials: credentials});
    };

    login = event => {

        if (this.state.isLogin) {
            fetch(`${process.env.REACT_APP_SERVER_URL}/auth/`, {
                method: 'POST',
                headers: {'Content-type': 'application/json'},
                body: JSON.stringify(this.state.credentials)
            }).then( res => res.json())
                .then(result => {
                    console.log(result);
                    this.props.cookies.set('token', result.token);
                    window.location.href = '/movies';
                })
                .catch( error => console.log(error))
        } else {
            fetch(`${process.env.REACT_APP_API_URL}/users/`, {
                method: 'POST',
                headers: {'Content-type': 'application/json'},
                body: JSON.stringify(this.state.credentials)
            }).then( res => res.json())
                .then(result => {
                    this.setState({isLogin: !this.state.isLogin})

                })
                .catch( error => console.log(error))
        }


    };

    toggleView = () => {
        this.setState({isLogin: !this.state.isLogin})
    };

    render() {

        const isDisabled = this.state.credentials.username.length === 0 ||
            this.state.credentials.password.length === 0;

        return (
            <div className="login-main">
                <h4>Welcome to Movie Rater</h4>
                <h1>
                    {this.state.isLogin ?
                        'Login'
                    :
                        'Register'
                    }
                    </h1>

                <div className="login-box">

                    <span>Username</span>
                    <input type="text"
                           className="input"
                           name="username"
                           value={this.state.credentials.username}
                           onChange={this.inputChanged} />

                    <span>Password</span>
                    <input type="password"
                           className="input"
                           name="password"
                           value={this.state.credentials.password}
                           onChange={this.inputChanged} />
                    <button className="add-button" disabled={isDisabled} onClick={this.login}>
                        {this.state.isLogin ?
                            'Login'
                            :
                            'Register'
                        }
                    </button>
                    <p onClick={this.toggleView} className="create-account">
                        {this.state.isLogin ?
                            'Create Account'
                            :
                            'Back to Login'
                        }
                    </p>
                </div>
            </div>
        )
    }
}

export default withCookies(Login);