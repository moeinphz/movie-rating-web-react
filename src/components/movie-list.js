import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function MovieList(props) {

    const movieClicked = movie => evt => {
        props.movieClicked(movie);
    };

    const editClicked = movie => {
        props.editClicked(movie)
    };

    const newMovie = () => {
        props.newMovie();
    };

    const state = {
        token: props.token
    };

    console.log(state)

    const data = Array.from(props.movies);

    const removeClicked = movie => {
        fetch(`${process.env.REACT_APP_API_URL}/movies/${movie.id}/`, {
            // fetch('http://127.0.0.1:8000/api/movies/', {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json',
                'Authorization': 'Token ${state.token}'
            }
        }).then( res => props.movieDeleted(movie))
            .catch( error => console.log(error))

    };

    return (
        <div>
            { data.map(movie => {
                return (
                    <div key={movie.id} className="movie-item">
                        <h3 onClick={movieClicked(movie)} className="clickable-item">
                            {movie.title}
                        </h3>

                        <FontAwesomeIcon icon="edit"
                                         className="clickable-icon"
                                         onClick={() => editClicked(movie)} />

                        <FontAwesomeIcon icon="trash"
                                         className="clickable-icon"
                                         onClick={() => removeClicked(movie)} />

                    </div>
                )
            })}
            <button onClick={newMovie} className="add-button">Add new Movie</button>
        </div>
    );
}

export default MovieList;