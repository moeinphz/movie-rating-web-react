import React, { Component } from'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
// var FontAwesome = require('react-fontawesome');


class MovieDetails extends Component {

    state = {
        highlighted: -1
    };

    highlightRate = high => evnt => {
        this.setState({highlighted: high})
    };

    rateClicked = stars => event => {
        fetch(`${process.env.REACT_APP_API_URL}/movies/${this.props.selectedMovie.id}/rate_movie/`, {
            // fetch('http://127.0.0.1:8000/api/movies/', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Token ${this.props.token}`
            },
            body: JSON.stringify({stars: stars + 1})
        }).then( res => res.json())
            .then(result => this.getDetails())
            .catch( error => console.log(error))
    };

    getDetails = () => {
        fetch(`${process.env.REACT_APP_API_URL}/movies/${this.props.selectedMovie.id}/`, {
            // fetch('http://127.0.0.1:8000/api/movies/', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Token ${this.props.token}`
            }
        }).then( res => res.json())
            .then(result => this.props.updateMovie(result))
            .catch( error => console.log(error))
    };


    render() {
        const mov = this.props.selectedMovie;

        return (
            <React.Fragment>
                {mov ? (
                    <div className="movie-details">
                        <h3>{mov.title}</h3>
                        <h3>{mov.average_rating}</h3>
                        <FontAwesomeIcon icon="star" className={mov.average_rating > 0 ? 'orange' : ''} />
                        <FontAwesomeIcon icon="star" className={mov.average_rating > 1 ? 'orange' : ''} />
                        <FontAwesomeIcon icon="star" className={mov.average_rating > 2 ? 'orange' : ''} />
                        <FontAwesomeIcon icon="star" className={mov.average_rating > 3 ? 'orange' : ''} />
                        <FontAwesomeIcon icon="star" className={mov.average_rating > 4 ? 'orange' : ''} />
                        ({mov.no_of_ratings})
                       <p>{mov.description}</p>

                        <div className="rate-container">
                            <h2>Rate it !!!</h2>
                            { [...Array(5)].map( (event, index) => {
                                return <FontAwesomeIcon key={index} icon="star"
                                                        className={this.state.highlighted > index - 1 ? 'purple' : ''}
                                                        onMouseEnter={this.highlightRate(index)}
                                                        onMouseLeave={this.highlightRate(-1)}
                                                        onClick={this.rateClicked(index)} />;
                            })}
                        </div>
                    </div>
                ) : null}
            </React.Fragment>

        )
    }
}

export default MovieDetails;