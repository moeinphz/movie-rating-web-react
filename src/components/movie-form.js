import React, { Component } from'react';
import '../App.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';


class MovieForm extends Component {

    state = {
        editedMovie: this.props.movie
    };

    inputChanged = event => {
        let movie = this.state.editedMovie;
        movie[event.target.name] = event.target.value;
        this.setState({editedMovie: movie});
    };

    saveClicked = () => {
        fetch(`${process.env.REACT_APP_API_URL}/movies/`, {
            // fetch('http://127.0.0.1:8000/api/movies/', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Token ${this.props.token}`
            },
            body: JSON.stringify(this.state.editedMovie)
        }).then( res => res.json())
            .then(result => this.props.newMovie(result))
            .catch( error => console.log(error))
    };

    updateClicked = () => {
        fetch(`${process.env.REACT_APP_API_URL}/movies/${this.props.movie.id}/`, {
            // fetch('http://127.0.0.1:8000/api/movies/', {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Token ${this.props.token}`
            },
            body: JSON.stringify(this.state.editedMovie)
        }).then( res => res.json())
            .then(result => this.props.updatedMovie(result))
            .catch( error => console.log(error))
    };



    cancelClicked = () => {
        this.props.cancelForm();
    };



    render() {
        // const mov = this.props.editedMovie;

        const isDisabled = this.state.editedMovie.title.length === 0 ||
            this.state.editedMovie.description.length === 0;

        return (
            <React.Fragment>
                <div className="form-container">

                    <span>Title</span>
                    <input type="text"
                           className="input"
                           name="title"
                           value={this.props.movie.title}
                           onChange={this.inputChanged} />

                    <span>Description</span>
                    <textarea className="input"
                              value={this.props.movie.description}
                              name="description"
                              onChange={this.inputChanged} />

                    { this.props.movie.id ?
                        <button className="add-button" disabled={isDisabled} onClick={this.updateClicked}>Update</button>
                        :
                        <button className="add-button" disabled={isDisabled} onClick={this.saveClicked}>Save</button>
                    }


                    <button className="cancel-button" onClick={this.cancelClicked}>Cancel</button>
                </div>

            </React.Fragment>

        )
    }
}

export default MovieForm;