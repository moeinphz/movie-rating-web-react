import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faCoffee, faStar, faEdit, faTrash, faFilm } from '@fortawesome/free-solid-svg-icons'

library.add(faUser, faCoffee, faStar, faEdit, faTrash, faFilm);