import React, {Component} from 'react';
import './shared/FontAwesomeIcons'
import './App.css';
import MovieList from './components/movie-list';
import MovieDetails from './components/movie-details';
import MovieForm from './components/movie-form';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {withCookies} from 'react-cookie';

class App extends Component {

    state = {
        movies: [],
        selectedMovie: null,
        editedMovie: null,
        token: this.props.cookies.get('token')
    };

    movieClicked = movie => {
        this.setState({selectedMovie: movie, editedMovie: null});
    };

    movieUpdated = movie => {
        const movies = this.state.movies;
        movies.find(m => m.id === movie.id).average_rating = movie.average_rating;
        movies.find(m => m.id === movie.id).no_of_ratings = movie.no_of_ratings;
        this.setState({movies: movies, selectedMovie: movie});
    };

    editClicked = movie => {
        this.setState({editedMovie: movie})
    };

    newMovie = () => {
        this.setState({editedMovie: {title: '', description: ''}});
    };

    movieDeleted = movie => {
        const movies = this.state.movies.filter(m => m.id !== movie.id);
        this.setState({movies: movies, selectedMovie: null})
    };

    cancelForm = () => {
        this.setState({editedMovie: null})
    };

    addMovie = movie => {
        this.setState({movies: [...this.state.movies, movie]});
    };

  componentDidMount() {
      // get cookies
      if (this.state.token) {
          // fetch data
          // # Argument 1: URL
          // # Argument 2: Options as an Object
          fetch(`${process.env.REACT_APP_API_URL}/movies/`, {
              // fetch('http://127.0.0.1:8000/api/movies/', {
              method: 'GET',
              headers: {
                  'Authorization': `Token ${this.state.token}`
              }
          }).then( res => res.json())
              .then(mvs => this.setState({movies: mvs}))
              .catch( error => console.log(error))
      } else {
          window.location.href = '/'
      }
  }






  // render

    render() {
      return (
          <div className="App">
              <h1>
                  <FontAwesomeIcon icon="film" className="main-title-icon"/>
                  <span className="main-title">Movie Rater</span>
              </h1>


              <div className="layout">
                  <MovieList movies={this.state.movies}
                             token={this.state.token}
                             movieClicked={this.movieClicked}
                             newMovie={this.newMovie}
                             editClicked={this.editClicked}
                             movieDeleted={this.movieDeleted}/>
                  <div>
                      { this.state.editedMovie ?
                          <MovieForm movie={this.state.editedMovie}
                                     token={this.state.token}
                                     newMovie={this.addMovie}
                                     updatedMovie={this.movieClicked}
                                     cancelForm={this.cancelForm} />
                      :
                          <MovieDetails selectedMovie={this.state.selectedMovie}
                                        token={this.state.token}
                                        updateMovie={this.movieUpdated}/>
                      }


                  </div>

              </div>
          </div>
      )
  };
}

export default withCookies(App);
